from rest_framework import serializers
from api.models import *

class AlquileresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alquileres
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.fecha_devolucion = validated_data.get('fecha_devolucion', instance.fecha_devolucion)
        instance.precio = validated_data.get('precio', instance.precio)
        instance.save()
        return instance
  

class PeliculasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Peliculas
        fields = '__all__'
    
class DvdsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dvds
        fields = '__all__'

class ClientesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clientes
        fields = '__all__'
    
    def update(self, instance, validated_data):
        instance.numero_telefono = validated_data.get('numero_telefono', instance.numero_telefono)
        instance.correo = validated_data.get('correo', instance.correo)
        instance.save()
        return instance

class GeneroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Generos
        fields = '__all__'
