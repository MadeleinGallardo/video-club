from django.contrib import admin
from .models import *

# Register your models here.

class GenerosAdmin(admin.ModelAdmin):
    ordering = ('id', 'codigo',)
    list_display = ('id', 'codigo', 'descripcion')
    search_fields = ('id', 'codigo', 'descripcion',)
    list_filter = ('codigo',)

class PeliculasAdmin(admin.ModelAdmin):
    ordering = ('id', 'titulo', 'director', 'genero_id', 'estreno')
    list_display = ('id', 'titulo', 'director', 'genero_id', 'estreno')
    search_fields = ('id', 'titulo', 'director', 'genero_id__id', 'estreno')
    list_filter = ('genero_id'), 

class DvdsAdmin(admin.ModelAdmin):
    ordering = ('id', 'identificador_dvd', 'pelicula_id', 'disponible')
    list_display = ('id', 'identificador_dvd', 'pelicula_id', 'disponible')
    search_fields = ('id', 'identificador_dvd', 'pelicula_id__id')
    list_filter = ('disponible'), 

class ClientesAdmin(admin.ModelAdmin):
    ordering = ('id', 'dni', 'nombre', 'apellido', 'direccion', 'numero_telefono', 'correo')
    list_display = ('id', 'dni', 'nombre', 'apellido', 'direccion', 'numero_telefono', 'correo')
    search_fields = ('id', 'dni', 'nombre', 'apellido', 'direccion', 'numero_telefono', 'correo')

class AlquileresAdmin(admin.ModelAdmin):
    ordering = ('id', 'nro_recibo', 'dni_cliente', 'fecha_alquiler', 'fecha_devolucion', 'precio', 'dvd_id')
    list_display = ('id', 'nro_recibo', 'dni_cliente', 'fecha_alquiler', 'fecha_devolucion', 'precio', 'dvd_id')
    search_fields = ('id', 'nro_recibo', 'dni_cliente__dni', 'fecha_alquiler', 'fecha_devolucion', 'precio', 'dvd_id__id')


admin.site.register(Generos, GenerosAdmin)
admin.site.register(Peliculas, PeliculasAdmin)
admin.site.register(Dvds, DvdsAdmin)
admin.site.register(Clientes, ClientesAdmin)
admin.site.register(Alquileres, AlquileresAdmin)