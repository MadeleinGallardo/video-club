from django.db import models
from datetime import date

# Create your models here.

class Generos(models.Model):
    id = models.IntegerField(primary_key=True)
    codigo = models.CharField(max_length=100, null=False, blank= False)
    descripcion = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.id)

class Peliculas(models.Model):
    id = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=50, null=False, blank=False)
    director = models.CharField(max_length=50)
    genero_id = models.ForeignKey(Generos, on_delete=models.CASCADE)
    estreno = models.DateField()

    def __str__(self):
        return self.titulo

class Dvds(models.Model):
    id = models.IntegerField(primary_key=True)
    identificador_dvd = models.CharField(max_length=50, null=False, blank=False)
    pelicula_id = models.ForeignKey(Peliculas, on_delete=models.CASCADE)
    disponible = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)

class Clientes(models.Model):
    id= models.IntegerField(primary_key=True)
    dni = models.CharField(max_length=15, unique=True, blank=False, null=False)
    nombre = models.CharField(max_length=150, blank=False, null=False)
    apellido = models.CharField(max_length=150, blank=False, null=False)
    direccion = models.CharField(max_length=255)
    numero_telefono = models.CharField(verbose_name= 'Numero de telefono', max_length=15, unique=True)
    correo = models.EmailField(unique=True, blank=False, null=False)

    def __str__(self):
        return self.dni

class Alquileres(models.Model):
    id= models.IntegerField(primary_key=True)
    nro_recibo = models.CharField(max_length=150)
    dni_cliente = models.ForeignKey(Clientes, on_delete=models.CASCADE)
    fecha_alquiler = models.DateField(default=date.today)
    fecha_devolucion = models.DateField(blank=True, null=True)
    precio = models.DecimalField(max_digits=15, decimal_places=2, blank=True, default=0)
    dvd_id = models.ForeignKey(Dvds, on_delete=models.CASCADE)

    def __str__(self):
        return self.nro_recibo
    




