from django.urls import path
from api.api import *

urlpatterns = [
    path('alquileres/', alquiler_api_view, name = 'alquileres_api'),
    path('alquileres/<str:dni>/', alquiler_detail_view_dni, name ='alquiler_por_dni'),
    path('alquileres_filtrado_fecha/<str:fecha>/', alquiler_detail_view_fecha, name ='alquiler_por_fecha'),
    path('peliculas/', peliculas_api_view, name = 'peliculas_api'),
    path('peliculas/<int:genero_id>/', pelicula_detail_view, name ='pelicula_detalle'),
    path('pelicula_delete/<int:id>/', pelicula_delete, name ='pelicula_delete'),
    path('dvds/', dvds_api_view, name = 'dvd_api'),
    path('cliente/', clientes_api_view, name = 'cliente_api'),
    path('cliente/<str:dni>/', clientes_api_view_detail, name = 'cliente_api_detalle'),
    path('genero_delete/<str:codigo>/', genero_api_delete, name = 'genero_delete'),
]