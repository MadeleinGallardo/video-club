from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from datetime import datetime
from api.models import *
from api.serializers import *

@api_view(['GET'])
def alquiler_api_view(request):
    if request.method == 'GET':
        alquileres = Alquileres.objects.all()
        alquileres_serializer = AlquileresSerializer(alquileres, many = True)
    return Response(alquileres_serializer.data, status=status.HTTP_200_OK)

@api_view(['GET', 'PATCH'])
def alquiler_detail_view_dni(request, dni=None):


        if request.method == 'GET':
            alquiler = Alquileres.objects.filter(dni_cliente__dni=dni)
            alquiler_serializer = AlquileresSerializer(alquiler, many = True)
            return Response(alquiler_serializer.data, status=status.HTTP_200_OK)
        elif request.method == 'PATCH':
            alquiler = Alquileres.objects.filter(dni_cliente__dni=dni, fecha_devolucion = None)
            alquileres = []
            if alquiler:
                for alq in alquiler:
                    alquiler_serializer = AlquileresSerializer(alq,data=request.data, partial=True)
                    if alquiler_serializer.is_valid():
                        alquiler_serializer.save()
                        alquileres.append(
                            {
                                "id": alq.id,
                                "nro_recibo": alq.nro_recibo,
                                "fecha_alquiler": alq.fecha_alquiler,
                                "fecha_devolucion": alq.fecha_devolucion,
                                "precio": alq.precio,
                                "dni_cliente": alq.dni_cliente.dni,
                                "dvd_id": alq.dvd_id.id
                            }
                        )
                return Response(alquileres, status=status.HTTP_200_OK)
            return Response({'message': 'No se encontraron registros de alquileres'}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PATCH'])
def alquiler_detail_view_fecha(request,fecha=None):
    fecha_dt = datetime.strptime(fecha, '%Y-%m-%d')
    fecha_dt = fecha_dt.date()

    if request.method == 'GET':
        alquiler = Alquileres.objects.filter(fecha_alquiler=fecha_dt)
        alquiler_serializer = AlquileresSerializer(alquiler, many = True)
        return Response(alquiler_serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'PATCH':
        alquiler = Alquileres.objects.filter(fecha_alquiler=fecha_dt,fecha_devolucion = None)
        alquileres = []
        if alquiler:
            for alq in alquiler:
                alquiler_serializer = AlquileresSerializer(alq,data=request.data, partial=True)
                if alquiler_serializer.is_valid():
                    alquiler_serializer.save()
                    alquileres.append(
                        {
                            "id": alq.id,
                            "nro_recibo": alq.nro_recibo,
                            "fecha_alquiler": alq.fecha_alquiler,
                            "fecha_devolucion": alq.fecha_devolucion,
                            "precio": alq.precio,
                            "dni_cliente": alq.dni_cliente.dni,
                            "dvd_id": alq.dvd_id.id
                        }
                    )
            return Response(alquileres, status=status.HTTP_200_OK)
        return Response({'message': 'No se encontraron registros de alquileres'}, status=status.HTTP_400_BAD_REQUEST)
                 

@api_view(['GET', 'POST'])
def peliculas_api_view(request):
    if request.method == 'GET':
        peliculas = Peliculas.objects.all()
        peliculas_serializer = PeliculasSerializer(peliculas, many = True)
        return Response(peliculas_serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        peliculas_serializer = PeliculasSerializer(data = request.data)
        if peliculas_serializer.is_valid():
            peliculas_serializer.save()
            return Response(peliculas_serializer.data, status=status.HTTP_201_CREATED)
        return Response(peliculas_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def pelicula_detail_view(request, genero_id=None):
    pelicula = Peliculas.objects.filter(genero_id__id=genero_id)
    if pelicula:
        if request.method == 'GET':
            pelicula_serializer = PeliculasSerializer(pelicula, many = True)
            return Response(pelicula_serializer.data, status=status.HTTP_200_OK)

@api_view(['DELETE'])
def pelicula_delete(request, id):
    pelicula = Peliculas.objects.get(id=id)
    if request.method == 'DELETE':
            pelicula.delete()
            return Response({'message': 'Usuario eliminado correctamente'}, status=status.HTTP_200_OK)
    return Response({'message:' 'No se ha encontrado el registro'}, status=status.HTTP_400_BAD_REQUEST) 


    
@api_view(['GET', 'POST'])
def dvds_api_view(request):
    if request.method == 'GET':
        dvds = Dvds.objects.all()
        dvds_serializer = DvdsSerializer(dvds, many = True)
        serializer_data = dvds_serializer.data
        response = []
        for dvd in serializer_data:
            response_dvd = {
                "id": dvd['id'],
                "identificador_dvd": dvd['identificador_dvd'],
                "disponible": dvd['disponible'],
                "pelicula_id": dvd['pelicula_id'],
                "pelicula": []
            }
            pelicula = Peliculas.objects.get(id=dvd['pelicula_id'])
            response_dvd['pelicula'].append({
                "id": pelicula.id,
                "titulo": pelicula.titulo,
                "director": pelicula.director,
                "estreno": pelicula.estreno,
                "genero_id": pelicula.genero_id.id
            })
            response.append(response_dvd)
        return Response(response, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        dvds_serializer = DvdsSerializer(data = request.data)
        if dvds_serializer.is_valid():
            dvds_serializer.save()
            return Response(dvds_serializer.data, status=status.HTTP_201_CREATED)
        return Response(dvds_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET','POST'])
def clientes_api_view(request):
    if request.method == 'GET':
        cliente = Clientes.objects.all()
        cliente_serializer = ClientesSerializer(cliente, many = True)
        return Response(cliente_serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        cliente_serializer = ClientesSerializer(data = request.data)
        if cliente_serializer.is_valid():
            cliente_serializer.save()
            return Response(cliente_serializer.data, status=status.HTTP_201_CREATED)
        return Response(cliente_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PATCH'])
def clientes_api_view_detail(request, dni=None):
    if request.method == 'GET':
        cliente = Clientes.objects.filter(dni=dni).first()
        cliente_serializer = ClientesSerializer(cliente)
        return Response(cliente_serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'PATCH':
        cliente = Clientes.objects.filter(dni=dni).first()
        cliente_serializer = ClientesSerializer(cliente,data=request.data, partial=True)
        if cliente_serializer.is_valid():
            cliente_serializer.save()
            return Response(cliente_serializer.data, status=status.HTTP_200_OK)
        return Response(cliente_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def genero_api_delete(request, codigo=None):
    genero = Generos.objects.filter(codigo=codigo).first()
    if request.method == 'DELETE':
            genero.delete()
            return Response({'message': 'Usuario eliminado correctamente'}, status=status.HTTP_200_OK)
    return Response({'message:' 'No se ha encontrado el registro'}, status=status.HTTP_400_BAD_REQUEST) 





